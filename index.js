let array = [
  [2, 1, 1, 1],
  [1, 2, 3, 3],
  [1, 2, 2, 5],
  [1, 2, 3, 2],
];
let size = 3;
const arrayRun = (arr, query, k) => {
  let count = 0;
  let temp = [];
  for (let row = 0; row < arr.length; row++) {
    for (let col = 0; col < arr.length; col++) {
      if (arr[row][col] == query) {
        count++;
      }
    }
    if (count == k) {
      temp.push({ val: query, count: count, row: row });
    }
    count = 0;
  }
  if (temp.length != 0) return temp;
  else return -1;
};
const arrayRunCol = (arr, query, k) => {
  let count = 0;
  let temp = [];
  for (let col = 0; col < arr.length; col++) {
    for (let row = 0; row < arr.length; row++) {
      if (arr[row][col] == query) {
        count++;
      }
    }
    if (k == count) {
      temp.push({ val: query, count: count, col: col });
    }
    count = 0;
  }
  if (temp.length != 0) return temp;
  else return -1;
  //console.log(temp);
};
const arrarySearch = (arr, k) => {
  let temp = [];
  let res = [];
  let t = arr.map((x) => {
    x.map((y) => {
      if (temp.length == 0 || !temp.includes(y)) {
        temp.push(y);
        let val = arrayRun(arr, y, k);
        if (val != -1) res.push(val);
      }
    });
  });
  console.log("results: ", res);
  console.log("Least: ", findLeast(res));
};
const findLeast = (arr) => {
  let minValue = 0;
  let min;
  arr.map((x, index) => {
    if (minValue > x.count || index == 0) {
      min = x;
      minValue = x.count;
    }
  });
  return min;
};
const verticalSearch = (arr, k) => {
  let count = 0;
  let temp = [];
  let res = [];

  for (let row = 0; row < arr.length; row++) {
    for (let column = 0; column < arr[row].length; column++) {
      if (temp.length == 0 || !temp.includes(arr[row][column])) {
        temp.push(arr[row][column]);
        // count = arrayRun(arr, arr[row][column]);
        let val = arrayRunCol(arr, arr[row][column], k);

        if (val != -1) res.push(val);
      }
    }
  }
  return res;
};
const diagonalSearch = (arr, k) => {
  let temp = [];
  let res = [];
  let size = arr[0].length;
  let diagonal = 0;
  let count;
  for (let row = 0; row < size; row++) {
    for (let col = 0; col < size; col++) {
      if (col == diagonal) {
        // console.log(arr[row][col]);
        temp.push(arr[row][col]);
        diagonal++;
        break;
      }
    }
  }
  let temp2 = [];
  for (let n = 0; n < temp.length; n++) {
    if (temp.length == 0 || !temp2.includes(temp[n])) {
      let x = checkDub(temp[n], temp, k);
      if (x != -1) res.push({ val: x, count: k });
    }
    temp2.push(temp[n]);
  }
  console.log("diagonalSearch", res);
};
const checkDub = (val, arr, k) => {
  let count = 0;
  let temp;
  for (let x = 0; x < arr.length; x++) {
    if (val == arr[x]) {
      count++;
    }

    temp = x;
  }
  if (count == k) return val;
  else return -1;
};
console.log("-------Row Search-------");
arrarySearch(array, 3);
console.log("-------col Seacrch------");
console.log(verticalSearch(array, 2));
console.log("-------diagonal Seacrch------");
diagonalSearch(array, 4);
